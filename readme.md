#Server Configurator

Backend App for server configurator

## Install

1. copy `.env.example` to `.env`, then configure
2. add redis, simplexml extensions to php
3. `composer install`
4. Application Key: `php artisan key:generate`
5. create DB *homestead*, then `php artisan migrate`
6. `composer dump-autoload`, `php artisan db:seed`


## Install from Docker

1. [Install docker](https://www.docker.com/products/docker-desktop)
2. copy `.env.example` to `.env`, then configure (DB users root:root or user:user)
3. `docker-compose up -d` pull images, build and start containers
4. `docker-compose exec php-fpm composer install`
5. Application Key: `docker-compose exec php-fpm php artisan key:generate`
6. `docker-compose exec php-fpm php artisan migrate`
7. `docker-compose exec php-fpm php artisan db:seed`
8. **API app**: localhost:8000/api/, **adminer** (like phpmyadmin): localhost:8080 login: root, password root, сервер mysql, db: homestead

## Console commands

Создать модель с миграцией `php artisan make:model Flight --migration`

Работа с API/JSON 
* ресурс (одна модель) `php artisan make:resource UserResource` (`return new UserResource(User::find(1));`)
* Коллекции (много моделей с пагинацией) `php artisan make:resource UserCollection` (`return new UserCollection(User::paginate());`)

Создать контроллер `php artisan make:controller PhotoController`, `php artisan make:controller Admin/UserController --resource --model=Entity\User`

Создать валидатор запроса `php artisan make:request StoreBlogPost`

Консольные команды `php artisan make:command SendEmails`

Миграции `php artisan migrate`, `php artisan migrate:fresh`, `php artisan make:migration create_cars_migration --create=cars`

Заполнить тестовыми данными `php artisan make:seeder UsersTableSeeder`, `php artisan db:seed --class=SettingsSeeder`

Консольные команды, специфичные для нашего проекта: `php artisan parse:hpe`, `php artisan shinservice:update`



