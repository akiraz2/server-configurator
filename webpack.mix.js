const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js').js('resources/js/app2.js', 'public/js').extract(['vue', 'lodash', 'element-ui', 'axios', 'jquery', 'popper.js']);
mix.sass('resources/sass/app.scss', 'public/css');
mix.browserSync('localhost');
mix.disableNotifications();
