<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $brand = \App\Brand::create(['title' => 'HPE']);

        $hpeFamilies = ['MicS', 'ML30', 'ML110','ML350', 'DL20', 'DL360', 'DL380', 'DL560', 'DL580'];
        foreach ($hpeFamilies as $family) {
            \App\Family::create(['title' => $family, 'brand_id' => $brand->id]);
        }
    }
}
