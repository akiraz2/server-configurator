<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductOptionFamiliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_option_families', function (Blueprint $table) {
            $table->integer('id', true, true);
            $table->integer('product_option_id', false, true)->index();
            $table->integer('family_id', false, true)->index();
        });
        Schema::table('product_option_families', function (Blueprint $table) {
            $table->foreign('product_option_id')->references('id')->on('product_options')->onDelete('cascade');
        });
        Schema::table('product_option_families', function (Blueprint $table) {
            $table->foreign('family_id')->references('id')->on('families')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_option_families');
    }
}
