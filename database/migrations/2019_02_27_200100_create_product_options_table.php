<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_options', function (Blueprint $table) {
            $table->integer('id', true, true);
            $table->string('pn_id')->unique()->index();
            $table->integer('brand_id', false, true);
            $table->integer('cat1_id', false, true);
            $table->integer('cat2_id', false, true)->nullable();
            $table->integer('cat3_id', false, true)->nullable();
            $table->string('title');
            $table->string('requires')->nullable();
            $table->string('comment')->nullable();
            $table->decimal('price')->default(0);
        });
        Schema::table('product_options', function (Blueprint $table) {
            $table->foreign('cat1_id')->references('id')->on('categories')->onDelete('cascade');
        });
        Schema::table('product_options', function (Blueprint $table) {
            $table->foreign('cat2_id')->references('id')->on('categories')->onDelete('cascade');
        });
        Schema::table('product_options', function (Blueprint $table) {
            $table->foreign('cat3_id')->references('id')->on('categories')->onDelete('cascade');
        });
        Schema::table('product_options', function (Blueprint $table) {
            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_options');
    }
}
