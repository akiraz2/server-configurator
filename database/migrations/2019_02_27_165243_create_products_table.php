<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->integer('id', true, true);
            $table->string('pn_id')->unique()->index();
            $table->string('title');
            $table->integer('brand_id', false, true);
            $table->integer('family_id', false, true);
            $table->integer('form_factor_id', false, true);

            $table->string('cpu')->default(null)->nullable();
            $table->tinyInteger('cpu_inst_qty')->default(1);
            $table->tinyInteger('cpu_max_qty')->default(1);

            $table->string('memory_inst')->default(null)->nullable();
            $table->tinyInteger('memory_inst_qty')->default(0);
            $table->tinyInteger('memory_slot_qty')->default(0);
            $table->tinyInteger('memory_slot_qty_max')->default(0);

            $table->string('raid')->default(null)->nullable();
            $table->string('raid_battery')->default(null)->nullable();

            $table->enum('drive_type', ['LFF', 'SFF'])->default('LFF');
            $table->boolean('drive_hot_plug')->default(false);
            $table->string('drive_inst')->default(null)->nullable();
            $table->tinyInteger('drive_inst_qty')->default(null)->nullable();
            $table->tinyInteger('drive_slot_qty')->default(null)->nullable();
            $table->tinyInteger('drive_slot_qty_max')->default(null)->nullable();

            $table->string('optical_drive_inst')->default(null)->nullable();

            $table->string('universal_media_bay')->default(null)->nullable();

            $table->string('psu')->default(null)->nullable();
            $table->tinyInteger('psu_inst')->default(1);
            $table->tinyInteger('psu_max_qty')->default(1);

            $table->tinyInteger('pcie_qty')->default(1);
            $table->tinyInteger('pcie_qty_max')->default(1);

            $table->string('flr')->default(null)->nullable();

            $table->string('ilo')->default(null)->nullable();

            $table->string('cable_mng')->default(null)->nullable();

            $table->string('rails')->default(null)->nullable();

            $table->string('comment')->default(null)->nullable();

            $table->decimal('price')->default(0);
        });
        Schema::table('products', function (Blueprint $table) {
            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
        });
        Schema::table('products', function (Blueprint $table) {
            $table->foreign('family_id')->references('id')->on('families')->onDelete('cascade');
        });
        Schema::table('products', function (Blueprint $table) {
            $table->foreign('form_factor_id')->references('id')->on('form_factors')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
