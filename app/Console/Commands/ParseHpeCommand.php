<?php

namespace App\Console\Commands;

use App\Parsers\ParserHPE;
use Illuminate\Console\Command;

class ParseHpeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:hpe';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse HPE excel file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $parser = new ParserHPE();
        return $parser->start();
    }
}
