<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOptionFamily extends Model
{
    public $timestamps = false;

    protected $fillable = ['product_option_id', 'family_id'];
}
