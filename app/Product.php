<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'brand_id',
        'family_id',
        'pn_id',
        'title',
        'form_factor_id',

        'cpu',
        'cpu_inst_qty',
        'cpu_max_qty',

        'memory_inst',
        'memory_inst_qty',
        'memory_slot_qty',
        'memory_slot_qty_max',

        'raid',
        'raid_battery',

        'drive_type',
        'drive_hot_plug',
        'drive_inst',
        'drive_inst_qty',
        'drive_slot_qty',
        'drive_slot_qty_max',

        'optical_drive_inst',

        'universal_media_bay',

        'psu',
        'psu_inst',
        'psu_max_qty',

        'pcie_qty',
        'pcie_qty_max',

        'flr',

        'ilo',

        'cable_mng',

        'rails',

        'comment',
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    public function formFactor()
    {
        return $this->belongsTo('App\FormFactor');
    }

    public function family()
    {
        return $this->belongsTo('App\Family');
    }

    public function productOptions()
    {
        return $this->belongsToMany('App\ProductOption', 'product_option_families',  'family_id', 'product_option_id');
    }
}
