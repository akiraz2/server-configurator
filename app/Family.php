<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    public $timestamps = false;

    protected $fillable = ['title'];

    //protected $appends = ['form_factor_ids'];

    public function formFactors()
    {
        return $this->belongsToMany('App\FormFactor', 'products')->distinct('id');
    }

    public function getFormFactorIdsAttribute()
    {
        return $this->formFactors->pluck('id');
    }
}
