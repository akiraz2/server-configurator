<?php
/**
 * Created by PhpStorm.
 * User: u3456
 * Date: 27.02.2019
 * Time: 20:03
 */

namespace App\Parsers;


use App\Category;
use App\Family;
use App\FormFactor;
use App\Product;
use App\ProductOption;
use App\ProductOptionFamily;
use Illuminate\Support\Str;

class ParserHPE extends BaseParser
{
    public $filename = 'app/hpe/hpe-nomenclatura.xlsx';

    private $brand_id = 1;

    protected static $convert = [
        'pn_id' => 1,
        'title' => 2,
        'form_factor_id' => 3,

        'cpu' => 4,
        'cpu_inst_qty' => 5,
        'cpu_max_qty' => 6,

        'memory_inst' => 7,
        'memory_inst_qty' => 8,
        'memory_slot_qty' => 9,
        'memory_slot_qty_max' => 10,

        'raid' => 11,
        'raid_battery' => 12,

        'drive_type' => 13,
        'drive_hot_plug' => 14,
        'drive_inst' => 15,
        'drive_inst_qty' => 16,
        'drive_slot_qty' => 17,
        'drive_slot_qty_max' => 18,

        'optical_drive_inst' => 19,

        'universal_media_bay' => 20,

        'psu' => 21,
        'psu_inst' => 22,
        'psu_max_qty' => 23,

        'pcie_qty' => 24,
        'pcie_qty_max' => 25,

        'flr' => 26,

        'ilo' => 27,

        'cable_mng' => 28,

        'rails' => 29,

        'comment' => 30,
    ];

    public function start()
    {
        $inputFileName = @storage_path($this->filename);

        /** Load $inputFileName to a Spreadsheet object **/
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
        /*foreach ($spreadsheet->getAllSheets() as $sheet) {
            echo $sheet;
        }*/

        $this->processAllOptions($spreadsheet->getSheetByName('ALL options'));


        $hpeFamilies = Family::where('brand_id', 1)->get();

        $worksheet = $spreadsheet->getSheetByName($hpeFamilies[0]->title);
        $this->processWorksheet($worksheet, $hpeFamilies[0]->id);

        $worksheet = $spreadsheet->getSheetByName($hpeFamilies[0]->title . ' G10');
        $this->processOptions($worksheet, $hpeFamilies[0]);


        $worksheet = $spreadsheet->getSheetByName($hpeFamilies[1]->title);
        $this->processWorksheet($worksheet, $hpeFamilies[1]->id);

        $worksheet = $spreadsheet->getSheetByName($hpeFamilies[1]->title . ' G10');
        $this->processOptions($worksheet, $hpeFamilies[1]);


        $worksheet = $spreadsheet->getSheetByName($hpeFamilies[2]->title);
        $this->processWorksheet($worksheet, $hpeFamilies[2]->id);

        $worksheet = $spreadsheet->getSheetByName($hpeFamilies[2]->title . ' G10');
        $this->processOptions($worksheet, $hpeFamilies[2]);


        $worksheet = $spreadsheet->getSheetByName($hpeFamilies[3]->title);
        $this->processWorksheet($worksheet, $hpeFamilies[3]->id);

        $worksheet = $spreadsheet->getSheetByName($hpeFamilies[3]->title . ' G10');
        $this->processOptions($worksheet, $hpeFamilies[3]);


//        //товары каждой линейки
//        foreach ($hpeFamilies as $family) {
//            $worksheet = $spreadsheet->getSheetByName($family->title);
//            $this->processWorksheet($worksheet, $family->id);
//        }
//        //опции для каждой линейки
//        foreach ($hpeFamilies as $family) {
//            $worksheet = $spreadsheet->getSheetByName($family->title.' G10');
//            $this->processOptions($worksheet, $family);
//        }
    }

    protected function processAllOptions($worksheet)
    {
        $highestRow = $worksheet->getHighestRow();
        for ($row = 2; $row <= $highestRow; ++$row) {
            $pnId = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
            if ($pnId) {
                $title = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
                $category = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
                ProductOption::updateOrCreate(
                    ['pn_id' => $pnId, 'brand_id' => $this->brand_id],
                    ['title' => $title, 'brand_id' => 1, 'cat1_id' => Category::firstOrCreate(['title' => $category])->id]
                );
            }
        }
    }

    protected function processWorksheet($worksheet, int $familyId)
    {
        if (!$worksheet) return;
        $highestRow = $worksheet->getHighestRow();
        for ($row = 2; $row <= $highestRow; ++$row) {
            $pnId = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
            if ($pnId) {
                $this->processRow($worksheet, $row, $familyId);
            }
        }
    }

    protected function processRow($worksheet, int $row, int $familyId)
    {
        $data = [
            'brand_id' => 1,
            'family_id' => $familyId,
        ];
        foreach (static::$convert as $param => $index) {
            $value = $worksheet->getCellByColumnAndRow($index, $row)->getValue();
            $method = 'transform' . Str::camel($param);
            $data[$param] = method_exists($this, $method) ? $this->$method($value) : $value;
        }
        Product::updateOrCreate(['pn_id' => $data['pn_id'], 'brand_id' => $this->brand_id], $data);
    }

    protected function processOptions($worksheet, Family $family)
    {
        if (!$worksheet) return;
        $highestRow = $worksheet->getHighestRow();
        for ($row = 2; $row <= $highestRow; ++$row) {
            $pnId = $worksheet->getCellByColumnAndRow(12, $row)->getValue();
            if ($pnId) {
                $title = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
                $cat1 = $worksheet->getCellByColumnAndRow(13, $row)->getValue();
                $cat2 = $worksheet->getCellByColumnAndRow(14, $row)->getValue();
                $cat3 = $worksheet->getCellByColumnAndRow(15, $row)->getValue();
                $requires = $worksheet->getCellByColumnAndRow(17, $row)->getValue();
                $comment = $worksheet->getCellByColumnAndRow(18, $row)->getValue();
                $productOption = ProductOption::updateOrCreate(
                    ['pn_id' => $pnId, 'brand_id' => $this->brand_id],
                    [
                        'pn_id' => $pnId,
                        'title' => $title,
                        'brand_id' => 1,
                        'cat1_id' => Category::firstOrCreate(['title' => $cat1])->id,
                    ]
                );
                if (!is_null($cat2)) {
                    $productOption->cat2_id = Category::firstOrCreate(['title' => $cat2])->id;
                }
                if (!is_null($cat3)) {
                    $productOption->cat3_id = Category::firstOrCreate(['title' => $cat3])->id;
                }
                if (!is_null($requires)) {
                    $productOption->requires = $requires;
                }
                if (!is_null($comment)) {
                    $productOption->comment = $comment;
                }
                $productOption->save();
                $productOptionFamily = ProductOptionFamily::firstOrCreate(['product_option_id' => $productOption->id, 'family_id' => $family->id]);
            }
        }
    }

    protected function transformFormFactorId($value)
    {
        return FormFactor::firstOrCreate(['title' => $value])->id;
    }

    protected function transformDriveHotPlug($value)
    {
        return $value == 'N' ? false : true;
    }
}
