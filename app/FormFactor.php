<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormFactor extends Model
{
    public $timestamps = false;

    protected $fillable = ['title'];
}
