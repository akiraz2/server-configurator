<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Family;
use App\FormFactor;
use App\Product;
use App\ProductOption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServerController extends Controller
{
    public function info()
    {
        return [
            'brands' => Brand::get(),
            'families' => Family::get(),
            'form_factories' => FormFactor::get(),
            //'products' => Product::get(),
            'compatible' => DB::table('products')
                ->select(['brand_id', 'family_id', 'form_factor_id', 'cpu_max_qty', 'drive_type', 'psu_max_qty', 'drive_hot_plug'])
                ->distinct(true)->get()
        ];
    }

    public function product(Request $request, int $id)
    {
        $model = Product::where('products.id', $id)
            ->join('families', 'products.family_id', '=', 'families.id')
            ->join('form_factors', 'products.form_factor_id', '=', 'form_factors.id')
            ->join('brands', 'products.brand_id', '=', 'brands.id')
            ->addSelect('products.*', 'families.title as family', 'form_factors.title as form_factor')
            ->first();
        $options = ProductOption::where('product_option_families.family_id', $model->family_id)
            ->addSelect(['product_options.*', 'categories.title as cat1', 'categories2.title as cat2', 'categories3.title as cat3'])
            ->join('product_option_families', 'product_options.id', '=', 'product_option_families.product_option_id')
            ->leftJoin('categories', 'categories.id', '=', 'product_options.cat1_id')
            ->leftJoin('categories as categories2', 'categories2.id', '=', 'product_options.cat2_id')
            ->leftJoin('categories as categories3', 'categories3.id', '=', 'product_options.cat3_id')
            ->get();

        return [
            'product' => $model,
            'options' => $options
        ];
    }
}
