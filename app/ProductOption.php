<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOption extends Model
{
    public $timestamps = false;

    protected $fillable = ['title', 'cat1_id', 'cat2_id', 'cat3_id','price', 'pn_id', 'brand_id'];

    public function families()
    {
        return $this->hasMany('App\ProductOptionFamily');
    }

    public function cat1()
    {
        return $this->belongsTo('App\Category');
    }

    public function cat2()
    {
        return $this->belongsTo('App\Category');
    }
}
