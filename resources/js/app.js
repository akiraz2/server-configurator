require('./bootstrap');
import 'element-ui/lib/theme-chalk/index.css';
const app = new Vue({
    el: '#app',
    data: {
        form: {
            brand_id: 1,
            family_id: [],
            form_factor_id: [],
            drive_type: [],
            cpu_max_qty: [],
            psu_max_qty: [],
            drive_hot_plug: []
        },
        brands: [],
        families: [],
        form_factors: [],
        compatible: [],
        products: []
    },
    computed: {
        filteredFamilies() {
            return Object.values(this.form).filter((formItemValue) => !_.isEmpty(formItemValue)).length ? this.families.filter((item) => item.brand_id === this.form.brand_id) : this.families;
        },
        /**
         * Те поля, которые кликнуты в фильтре и не нули
         * [brand_id, form_factory_id, ...]
         * @returns {string[]}
         */
        formFilledKeys() {
            return Object.keys(this.form).filter((key) => Array.isArray(this.form[key]) ? this.form[key].length : this.form[key]);
        },
        filteredCompatibleAll() {
            return this.filteredCompatible();
        },
        filteredCompatible() {
            return (without = '') => {
                if (!this.compatible.length) return [];
                const keysAll = Object.keys(this.form);//все доступные поля в таблице [brand_id, form_factory_id, ...]
                const keysNeedToFilter = _.intersection(keysAll, this.formFilledKeys);//те поля, которые нужно искать
                const filter = this.form;
                const filtered = this.compatible.filter((item) => {
                    for (const key in filter) {
                        if (without === '' || (without !== '' && key !== without)) {
                            if (item[key] === undefined) {
                                return false;
                            }
                            if (Array.isArray(filter[key])) {
                                if (filter[key].length && !filter[key].includes(item[key])) {
                                    return false;
                                }
                            } else if (item[key] !== filter[key]) {
                                return false;
                            }
                        }
                    }
                    return true;
                });
                return filtered;
            }
        },
        filteredCompatiblePluck() {
            return (key) => Array.from(new Set(this.filteredCompatible(key).map((item) => item[key])));
        },
        filteredCompatiblePluckFormFactor() {
            return this.filteredCompatiblePluck('form_factor_id');
        },
        filteredCompatiblePluckFamily() {
            return this.filteredCompatiblePluck('family_id');
        },
        filteredCompatiblePluckDriveType() {
            return this.filteredCompatiblePluck('drive_type');
        },
        filteredCompatiblePluckCpu() {
            return this.filteredCompatiblePluck('cpu_max_qty');
        }
    },
    methods: {
        checkDisabled(key, value) {
            return !this.filteredCompatiblePluck(key).includes(value);
        },
        checkFormFactory(formFactorId) {
            return !this.filteredCompatiblePluck('form_factor_id').includes(formFactorId);
        },
        checkFamily(familyId) {
            return !this.filteredCompatiblePluckFamily.includes(familyId);
        }
    },
    async mounted() {
        const result = await axios.get('/server/info');
        this.brands = result.data.brands;
        this.families = result.data.families;
        this.form_factors = result.data.form_factories;
        this.compatible = result.data.compatible;
    }
});
