require('./bootstrap');
import 'element-ui/lib/theme-chalk/index.css';
const app2 = new Vue({
    el: '#app2',
    data: {
        form: {
            memory: []
        },
        product_id: 8,
        product: {
            title: null,
            pn_id: null
        },
        options: []
    },
    computed: {
        memoryOptions() {
            return this.options.filter((option) => option.cat1 === 'Memory');
        },
        raidOptions() {
            return this.options.filter((option) => option.cat1.includes('Smart Array Controller'));
        }
    },
    methods: {

    },
    async mounted() {
        const result = await axios.get(`/server/product/${this.product_id}`);
        this.product = result.data.product;
        this.options = result.data.options;
    }
});
