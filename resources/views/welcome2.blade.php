<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:200,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            min-height: 100vh;
            margin: 0;
        }

        .full-height {
            min-height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">

    <div class="content" id="app2">
        <h1 class="title m-b-md">
            Конфигуратор серверов
        </h1>
        <h2>@{{product.title}}</h2>
        <h3>@{{ product.pn_id }}</h3>
        <el-form ref="form" :model="form" label-width="180px">

            <el-form-item>
                <el-button type="primary">Поиск</el-button>
                <el-button>Отмена</el-button>
            </el-form-item>
        </el-form>

    </div>
</div>
</body>
<script src="/js/manifest.js"></script>
<script src="/js/vendor.js"></script>
<script src="/js/app2.js"></script>
</html>
