<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:200,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Roboto', sans-serif;
            font-weight: 400;
            min-height: 100vh;
            margin: 0;
        }

        .full-height {
            min-height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">

    <div class="content" id="app">
        <h1 class="title m-b-md">
            Server Configurator
        </h1>
        <el-form ref="form" :model="form" label-width="180px">
            <el-form-item label="Производитель">
                <el-radio-group v-model="form.brand_id">
                    <el-radio :label="brand.id" v-for="brand in brands" :key="brand.id">
                        @{{brand.title}}
                    </el-radio>
                </el-radio-group>
            </el-form-item>

            <el-form-item label="Модели">
                <el-select
                        v-model="form.family_id"
                        multiple
                        collapse-tags
                        placeholder="Select">
                    <el-option
                            v-for="family in families"
                            :key="family.id"
                            :label="family.title"
                            :value="family.id"
                            :disabled="checkFamily(family.id)">
                    </el-option>
                </el-select>
            </el-form-item>

            <el-form-item label="Тип корпуса">
                <el-checkbox-group v-model="form.form_factor_id">
                    <el-checkbox v-for="form_factor in form_factors" :key="form_factor.id"
                                 :label="form_factor.id" :value="form_factor.id"
                                 :disabled="checkDisabled('form_factor_id',form_factor.id)">
                        @{{ form_factor.title }}
                    </el-checkbox>
                </el-checkbox-group>
            </el-form-item>

            <el-form-item label="Форм-фактор дисков">
                <el-checkbox-group v-model="form.drive_type">
                    <el-checkbox label="SFF" value="SFF" :disabled="checkDisabled('drive_type', 'SFF')">2.5</el-checkbox>
                    <el-checkbox label="LFF" value="LFF" :disabled="checkDisabled('drive_type', 'LFF')">3.5</el-checkbox>
                </el-checkbox-group>
            </el-form-item>

            <el-form-item label="Кол-во процессоров">
                <el-checkbox-group v-model="form.cpu_max_qty">
                    <el-checkbox :label="1" value="1" :disabled="checkDisabled('cpu_max_qty', 1)">1</el-checkbox>
                    <el-checkbox :label="2" value="2" :disabled="checkDisabled('cpu_max_qty', 2)">2</el-checkbox>
                    <el-checkbox :label="4" value="2" :disabled="checkDisabled('cpu_max_qty', 4)">4</el-checkbox>
                </el-checkbox-group>
            </el-form-item>

            <el-form-item label="Кол-во блоков питания">
                <el-checkbox-group v-model="form.psu_max_qty">
                    <el-checkbox :label="1" value="1" :disabled="checkDisabled('psu_max_qty', 1)">1</el-checkbox>
                    <el-checkbox :label="2" value="2" :disabled="checkDisabled('psu_max_qty', 2)">2</el-checkbox>
                </el-checkbox-group>
            </el-form-item>

            <el-form-item label="Тип подключения дисков">
                <el-checkbox-group v-model="form.drive_hot_plug">
                    <el-checkbox :label="0" value="0" :disabled="checkDisabled('drive_hot_plug', 0)">Диски без горячей замены</el-checkbox>
                    <el-checkbox :label="1" value="1" :disabled="checkDisabled('drive_hot_plug', 1)">Диски с горячей заменой</el-checkbox>
                </el-checkbox-group>
            </el-form-item>

            <el-form-item>
                <el-button type="primary">Поиск</el-button>
                <el-button>Отмена</el-button>
            </el-form-item>
        </el-form>

    </div>
</div>
</body>
<script src="/js/manifest.js"></script>
<script src="/js/vendor.js"></script>
<script src="/js/app.js"></script>
</html>
